package tasks2and3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;

public class Main {
    static long factorial(long x) {
        if (x == 1 || x == 0) {
            return 1;
        }
        return x * factorial(x - 1);
    }

    void sortFromFile() throws IOException {
        String content;
        content = new String(Files.readAllBytes(Paths.get( System.getProperty("user.dir") + "\\src\\tasks2and3\\row.txt")));
        ArrayList<Integer> list = new ArrayList<>();
        for (String numb : content.split(",")) {
            list.add(Integer.valueOf(numb));
        }
        System.out.println(list);
        Collections.sort(list);
        System.out.println(list);
        Collections.reverse(list);
        System.out.println(list);
    }

    public static void main(String[] args) throws IOException {
        System.out.println(factorial(20));
        new Main().sortFromFile();
    }
}
